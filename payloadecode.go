package payloadecode

import (
	"bytes"
	"encoding/json"
	"gitlab.com/project_falcon/kubeless/payload/payload"
	"strconv"
	"strings"
)

func decodeJSON(array []byte) (map[string]json.RawMessage, error) {
	raw := make(map[string]json.RawMessage)
	err := json.Unmarshal(array, &raw)

	return raw, err
}

func decodeExtraEnv(extraenv json.RawMessage) (map[string]string, error) {

	str := make(map[string]string)
	err := json.Unmarshal(extraenv, &str)

	return str, err
}

// DecodeDataJSON decode data
func DecodeDataJSON(arraya []byte) (payload.Data, error) {
	var dataD payload.Data
	var err error

	raw, err := decodeJSON(arraya)

	if err != nil {
		return dataD, err
	}

	// t, _ := time.Parse(time.RFC3339Nano, strings.ReplaceAll(string(raw["time"]), "\"", ""))
	build, _ := strconv.Atoi(string(raw["build"]))

	dataD = payload.Data{
		Idtask: strings.ReplaceAll(string(raw["idtask"]), "\"", ""),
		Branch: strings.ReplaceAll(string(raw["branch"]), "\"", ""),
		Repo:   strings.ReplaceAll(string(raw["repo"]), "\"", ""),
		Owner:  strings.ReplaceAll(string(raw["owner"]), "\"", ""),
		TTL:    strings.ReplaceAll(string(raw["ttl"]), "\"", ""),
		Build:  build,
		// ExtraEnv:  extraEnv,
		// Time:   t,
	}

	checkExtra, err := checkifExist(raw, "extraenv")
	if err != nil {
		return dataD, err
	}

	if !bytes.Equal(checkExtra, []byte("null")) {

		extraEnv, err := decodeExtraEnv(raw["extraenv"])
		if err != nil {
			return dataD, err
		}

		dataD.ExtraEnv = extraEnv
	}

	return dataD, err
}

//DecodeAPIJSON decode APIJSON
func DecodeAPIJSON(arraya []byte) (payload.APIData, error) {
	var api payload.APIData

	raw, err := decodeJSON(arraya)
	if err != nil {
		return api, err
	}

	ddata, err := DecodeDataJSON(raw["data"])
	if err != nil {
		ddata = payload.Data{}
	}

	checkConsul, err := checkifExist(raw, "consul")
	if err != nil {
		return api, err
	}

	if !bytes.Equal(checkConsul, []byte("null")) {
		consul, err := DecodeConsulJSON(raw["consul"])
		api.Consul = &consul

		if err != nil {
			return api, err
		}
	}

	checkRabbit, err := checkifExist(raw, "rabbitmq")
	if err != nil {
		return api, err
	}

	if !bytes.Equal(checkRabbit, []byte("null")) {
		rabbitmq, err := DecodeRabbitMQJSON(raw["rabbitmq"])
		api.RabbitMQ = &rabbitmq

		if err != nil {
			return api, err
		}
	}

	checkGit, err := checkifExist(raw, "git")
	if err != nil {
		return api, err
	}

	if !bytes.Equal(checkGit, []byte("null")) {
		git, err := DecodeGitJSON(raw["git"])

		if err != nil {
			return api, err
		}

		api.Git = &git
	}

	checkK8s, err := checkifExist(raw, "k8s")
	if err != nil {
		return api, err
	}

	if !bytes.Equal(checkK8s, []byte("null")) {

		k8s, err := DecodeK8SJSON(raw["k8s"])
		if err != nil {
			return api, err
		}

		api.K8s = &k8s
	}

	api.Data = &ddata

	api.Action = strings.ReplaceAll(string(raw["action"]), "\"", "")
	api.Res = strings.ReplaceAll(string(raw["res"]), "\"", "")
	api.Code, _ = strconv.Atoi(string(raw["code"]))
	api.Msg = strings.ReplaceAll(string(raw["msg"]), "\"", "")

	return api, err
}

// DecodeConsulJSON decode JSON
func DecodeConsulJSON(array []byte) (payload.ConsulData, error) {
	var consul payload.ConsulData

	raw, err := decodeJSON(array)
	if err != nil {
		return consul, err
	}

	build, _ := strconv.Atoi(string(raw["build"]))

	consul = payload.ConsulData{
		URL:        strings.ReplaceAll(string(raw["url"]), "\"", ""),
		Stage:      strings.ReplaceAll(string(raw["stage"]), "\"", ""),
		StartTime:  strings.ReplaceAll(string(raw["starttime"]), "\"", ""),
		EndTime:    strings.ReplaceAll(string(raw["endtime"]), "\"", ""),
		Status:     strings.ReplaceAll(string(raw["status"]), "\"", ""),
		EcrURL:     strings.ReplaceAll(string(raw["ecrurl"]), "\"", ""),
		PostDomain: strings.ReplaceAll(string(raw["postdomain"]), "\"", ""),

		Build: build,
	}

	checkBuilds, err := checkifExist(raw, "builds")
	if err != nil {
		return consul, err
	}

	if !bytes.Equal(checkBuilds, []byte("null")) {
		var bArray []string
		err = json.Unmarshal(raw["builds"], &bArray)

		if err != nil {
			return consul, err
		}

		consul.Builds = bArray
	}

	checkProjects, err := checkifExist(raw, "projects")
	if err != nil {
		return consul, err
	}

	if !bytes.Equal(checkProjects, []byte("null")) {
		var bArray []string
		err = json.Unmarshal(raw["projects"], &bArray)

		if err != nil {
			return consul, err
		}

		consul.Projects = bArray
	}

	checkStages, err := checkifExist(raw, "stages")
	if err != nil {
		return consul, err
	}

	if !bytes.Equal(checkStages, []byte("null")) {
		var bArray []string
		err = json.Unmarshal(raw["stages"], &bArray)

		if err != nil {
			return consul, err
		}

		consul.Stages = bArray
	}

	return consul, err
}

//DecodeGitJSON GIT JSON
func DecodeGitJSON(array []byte) (payload.GitData, error) {
	var git payload.GitData

	raw, err := decodeJSON(array)
	if err != nil {
		return git, err
	}

	var bArray []string
	err = json.Unmarshal(raw["branches"], &bArray)

	git = payload.GitData{
		Branches: bArray,
	}

	return git, err
}

//DecodeRabbitMQJSON decode rabbitmq
func DecodeRabbitMQJSON(array []byte) (payload.RabbitMQData, error) {
	var rabbit payload.RabbitMQData

	raw, err := decodeJSON(array)
	if err != nil {
		return rabbit, err
	}

	jobs, _ := strconv.Atoi(string(raw["queuejobs"]))

	rabbit = payload.RabbitMQData{
		QueueAction: strings.ReplaceAll(string(raw["queueaction"]), "\"", ""),
		QueueName:   strings.ReplaceAll(string(raw["queuename"]), "\"", ""),
		QueueJobs:   &jobs,
	}

	checkData, err := checkifExist(raw, "message")
	if err != nil {
		return rabbit, err
	}

	if !bytes.Equal(checkData, []byte("null")) {
		data, err := DecodeDataJSON(raw["message"])
		rabbit.Message = &data

		if err != nil {
			return rabbit, err
		}

	}

	return rabbit, err
}

//DecodeK8SJSON decode K8S JSON
func DecodeK8SJSON(array []byte) (payload.K8s, error) {
	var k8s payload.K8s

	raw, err := decodeJSON(array)
	if err != nil {
		return k8s, err
	}

	ahip, _ := strconv.ParseBool(string(raw["addhostingtopod"]))
	dryrun, _ := strconv.ParseBool(string(raw["dryrun"]))

	k8s = payload.K8s{
		FilterSearch:    strings.ReplaceAll(string(raw["filtersearch"]), "\"", ""),
		Namespace:       strings.ReplaceAll(string(raw["namespace"]), "\"", ""),
		AddHostIngToPod: ahip,
		DryRun:          dryrun,
	}

	checkPodList, err := checkifExist(raw, "pods")
	if err != nil {
		return k8s, err
	}

	if !bytes.Equal(checkPodList, []byte("null")) {

		podList, err := decodePods(raw["pods"])
		if err != nil {
			return k8s, err
		}

		k8s.Pods = podList
	}

	checkSvcList, err := checkifExist(raw, "services")
	if err != nil {
		return k8s, err
	}

	if !bytes.Equal(checkSvcList, []byte("null")) {

		svcList, err := decodeSvc(raw["services"])
		if err != nil {
			return k8s, err
		}

		k8s.Services = svcList
	}

	checkIngList, err := checkifExist(raw, "ingress")
	if err != nil {
		return k8s, err
	}

	if !bytes.Equal(checkIngList, []byte("null")) {

		ingList, err := decodeIng(raw["ingress"])
		if err != nil {
			return k8s, err
		}

		k8s.Ingress = ingList
	}

	checknodeList, err := checkifExist(raw, "nodes")
	if err != nil {
		return k8s, err
	}

	if !bytes.Equal(checknodeList, []byte("null")) {

		nodeList, err := decodeNode(raw["nodes"])
		if err != nil {
			return k8s, err
		}

		k8s.Nodes = nodeList
	}

	// k8s = K8s{
	// 	FilterSearch: strings.ReplaceAll(string(raw["filtersearch"]), "\"", ""),
	// }

	return k8s, err
}

func decodeNode(raw json.RawMessage) (*[]payload.Nodes, error) {
	var nodes *[]payload.Nodes

	err := json.Unmarshal(raw, &nodes)

	return nodes, err
}

func decodePods(raw json.RawMessage) (*[]payload.Pods, error) {
	var pods *[]payload.Pods
	err := json.Unmarshal(raw, &pods)

	return pods, err
}

func decodeSvc(raw json.RawMessage) (*[]payload.Services, error) {
	var svc *[]payload.Services
	err := json.Unmarshal(raw, &svc)

	return svc, err
}

func decodeIng(raw json.RawMessage) (*[]payload.Ingress, error) {
	var ing *[]payload.Ingress
	err := json.Unmarshal(raw, &ing)

	return ing, err
}

func checkifExist(raw map[string]json.RawMessage, field string) ([]byte, error) {

	check, err := raw[field].MarshalJSON()

	return check, err
}
